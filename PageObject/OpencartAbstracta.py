import time

from selenium import webdriver

base_url = "http://opencart.abstracta.us/"

class HomePage:
    def __init__(self, driver):
        self.driver = driver
        self.input_iphone = '/html/body/header/div/div/div[2]/div/input'
        self.boton_buscar = '//*[@id="search"]/span/button'
        self.seleccion_iphone = '//*[@id="content"]/div[3]/div/div/div[1]/a'
        self.agregar_iphone_carro = '//*[@id="button-cart"]'
        self.revisar_iphone_carro = '/html/body/header/div/div/div[3]/div/button'
        self.revisar_iphone_carro_2 = '/html/body/header/div/div/div[3]/div/ul/li[2]/div/p/a[1]'
        self.tabla_datos_iphone = '//*[@id="content"]/form/div/table/tbody/tr'
        self.borrar_iphone_carro = '//*[@id="content"]/form/div/table/tbody/tr/td[4]/div/span/button[2]'
        self.verifica_borrado_iphone_carro = '//*[@id="content"]/div/div/a'
        self.verifica_borrado_iphone_carro_2 = '//*[@id="cart-total"]'

    def get_input_iphone(self):
        return self.driver.find_element("xpath", self.input_iphone)

    def get_boton_buscar(self):
        return self.driver.find_element("xpath", self.boton_buscar)

    def get_seleccion_iphone(self):
        return self.driver.find_element("xpath", self.seleccion_iphone)

    def get_agregar_iphone_carro(self):
        return self.driver.find_element("xpath", self.agregar_iphone_carro)

    def get_revisar_iphone_carro(self):
        return self.driver.find_element("xpath", self.revisar_iphone_carro)

    def get_revisar_iphone_carro_2(self):
        return self.driver.find_element("xpath", self.revisar_iphone_carro_2)

    def get_lista_datos_iphone(self):
        return self.driver.find_element("xpath", self.tabla_datos_iphone).text

    def get_borrar_iphone_carro(self):
        return self.driver.find_element("xpath", self.borrar_iphone_carro)

    def get_verifica_borrado_iphone_carro(self):
        return self.driver.find_element("xpath", self.verifica_borrado_iphone_carro)

    def get_verifica_borrado_iphone_carro_2(self):
        return self.driver.find_element("xpath", self.verifica_borrado_iphone_carro_2)

    def busca_iphone(self):
        self.get_input_iphone().send_keys("iphone")
        self.get_boton_buscar().click()

    def seleciona_iphone_a_carro(self):
        self.get_seleccion_iphone().click()
        self.get_agregar_iphone_carro().click()
        self.driver.save_screenshot('screenshots/agregacarro.png')

    def revisa_carro(self):
        self.get_revisar_iphone_carro().click()
        time.sleep(2)
        self.get_revisar_iphone_carro_2().click()
        lista_datos_iphone = self.get_lista_datos_iphone().split()
        lista_datos_iphone.sort()
        list1 = ['iPhone', '***', 'product', '11', '$123.20', '$123.20']
        list1.sort()
        self.driver.save_screenshot('screenshots/Validacion_en_carro.png')
        assert list1 == lista_datos_iphone

    def borra_iphone_carro(self):
        self.driver.save_screenshot('screenshots/Validacion_borrado_carro_inicio.png')
        self.get_revisar_iphone_carro().click()
        time.sleep(2)
        self.get_revisar_iphone_carro_2().click()
        time.sleep(2)
        self.get_borrar_iphone_carro().click()
        time.sleep(2)
        self.get_verifica_borrado_iphone_carro().click()
        time.sleep(2)
        self.driver.save_screenshot('screenshots/Validacion_borrado_carro_final.png')
        list1 = self.get_verifica_borrado_iphone_carro_2().text.split()
        assert list1[0]=='0' and list1[3]=='$0.00'
