"Modulo de configuracion para Firefox's Selenium WebDriver"
import urllib3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FFOp

def setup():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    opt = FFOp()
    opt.add_argument('--headless')
    return webdriver.Firefox(options=opt)
