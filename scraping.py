from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import collections
from time import sleep
from selenium.webdriver.firefox.options import Options as FirefoxOptions

# Prueba sin navegador visual 
options = FirefoxOptions()
options.add_argument("--headless")
driver = webdriver.Firefox(options=options)

# Prueba con navegador visul 
#driver = webdriver.Firefox()
#driver.maximize_window()

driver.get("http://opencart.abstracta.us/")

sleep(2)
input_iphone = driver.find_element("xpath", '/html/body/header/div/div/div[2]/div/input')
# metodo ingreso iphone
input_iphone.send_keys("iphone")
driver.save_screenshot('Inicio.png')
boton_buscar = driver.find_element("xpath", '//*[@id="search"]/span/button').click()
#input_iphone.send_keys(Keys.RETURN)

sleep(2)
seleccion_iphone = driver.find_element("xpath", '//*[@id="content"]/div[3]/div/div/div[1]/a')
seleccion_iphone.send_keys(Keys.RETURN)

sleep(2)
agregar_iphone_carro = driver.find_element("xpath", '//*[@id="button-cart"]')
agregar_iphone_carro.send_keys(Keys.RETURN)

sleep(2)
revisar_iphone_carro = driver.find_element("xpath", '/html/body/header/div/div/div[3]/div/button')
revisar_iphone_carro.send_keys(Keys.RETURN)

sleep(2)                                                
revisar_iphone_carro_2 = driver.find_element("xpath", '/html/body/header/div/div/div[3]/div/ul/li[2]/div/p/a[1]')
revisar_iphone_carro_2.send_keys(Keys.RETURN)

#  Validar que el iPhone seleccionado se encuentre en el carrito de compras
sleep(2)
get_tabla_datos_iphone = driver.find_element("xpath", '//*[@id="content"]/form/div/table/tbody/tr').text
list = get_tabla_datos_iphone.split()
largo = len(list)
list1 = ['iPhone', '***', 'product', '11', '$123.20', '$123.20']

if (collections.Counter(list)==collections.Counter(list1)):
   print('Ok el Iphone se encuentra en la canasta ') 
else:
   print('Nok el Iphone no se encuentra en la canasta')
driver.save_screenshot('Validacion_en_carro.png')

sleep(2)
borrar_iphone_carro = driver.find_element("xpath", '//*[@id="content"]/form/div/table/tbody/tr/td[4]/div/span/button[2]')
borrar_iphone_carro.send_keys(Keys.RETURN)

sleep(2)
verifica_borrado_iphone_carro = driver.find_element("xpath", '//*[@id="content"]/div/div/a')
verifica_borrado_iphone_carro.send_keys(Keys.RETURN)

#  se valida lo solicitado
sleep(2)
verifica_borrado_iphone_carro_2 = driver.find_element("xpath", '//*[@id="cart-total"]').text
list = verifica_borrado_iphone_carro_2.split()
if list[0]=='0' and list[3]=='$0.00':
    print('Ok El iphone fue eliminado y no se muestra en carro')
else:
    print('Nok') 
driver.save_screenshot('Validacion_no_en_carro.png')

driver.close()


