"Test para Abstracta"
from WebDriver import Firefox
from PageObject.OpencartAbstracta import HomePage, base_url
import pytest
import time

@pytest.fixture
def webdriver():
    driver = Firefox.setup()
    driver.get(base_url)
    return driver

def test_revisa_carro(webdriver):
    home_page = HomePage(webdriver)
    home_page.busca_iphone()
    home_page.seleciona_iphone_a_carro()
    home_page.get_revisar_iphone_carro().click()
    time.sleep(2)
    home_page.get_revisar_iphone_carro_2().click()
    lista_datos_iphone = home_page.get_lista_datos_iphone().split()
    lista_datos_iphone.sort()
    list1 = ['iPhone', '***', 'product', '11', '$123.20', '$123.20']
    list1.sort()
    webdriver.save_screenshot('screenshots/Validacion_en_carro.png')
    assert list1 == lista_datos_iphone

def test_borra_iphone_carro(webdriver):
    home_page = HomePage(webdriver)
    home_page.busca_iphone()
    home_page.seleciona_iphone_a_carro()
    webdriver.save_screenshot('screenshots/Validacion_borrado_carro_inicio.png')
    home_page.get_revisar_iphone_carro().click()
    time.sleep(2)
    home_page.get_revisar_iphone_carro_2().click()
    time.sleep(2)
    home_page.get_borrar_iphone_carro().click()
    time.sleep(2)
    home_page.get_verifica_borrado_iphone_carro().click()
    time.sleep(2)
    webdriver.save_screenshot('screenshots/Validacion_borrado_carro_final.png')
    list1 = home_page.get_verifica_borrado_iphone_carro_2().text.split()
    assert list1[0]=='0' and list1[3]=='$0.00'
